package shahab.danesh;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import shahab.danesh.model.User;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "SettingsActivity";
    private EditText emailProfile;
    private EditText phoneProfile;
    private EditText addressProfile;
    private Button saveProfile;
    private TextView sendResetPasswordLink;
    private static final int REQUEST_CODE = 1234;
    private ProgressBar mProgressBar;

    ///var
    private boolean mStoragePermissions;
    private Uri mSelectedImageUri;
    private Bitmap mSelectedImageBitmap;
    private byte[] mBytes;
    private double progress;


    //firebase
    private FirebaseAuth.AuthStateListener mAuthListener;


    private DatabaseReference usersRef;
    private FirebaseAuth mAuth;

    private DatabaseReference rootRef;


    private boolean isEmpty(String string) {
        return string.equals("");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //////////
        rootRef = FirebaseDatabase.getInstance().getReference();

        mAuth = FirebaseAuth.getInstance();
        usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        emailProfile = findViewById(R.id.email_profile_id);
        phoneProfile = findViewById(R.id.phone_profile_id);
        addressProfile = findViewById(R.id.address_profile_id);
        saveProfile = findViewById(R.id.save_profile_id);
        sendResetPasswordLink=findViewById(R.id.reset_password_profile_id);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_profile_id);
        setupFirebaseAuth();
        setCurrentEmail();
//       init();

        retrieveUserInfo();


    }


    private void retrieveUserInfo() {

        rootRef.child("users").child(mAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String retrievePhoneNo=dataSnapshot.child("phone").getValue().toString();


                phoneProfile.setText(retrievePhoneNo);
                addressProfile.setText(dataSnapshot.child("address").getValue().toString());


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    private void setupFirebaseAuth() {
        Log.d(TAG, "setupFirebaseAuth: started.");

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in

                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
//                    toastMessage("Successfully signed in with: " + user.getEmail());


                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                    Toast.makeText(ProfileActivity.this, "Signed out", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ProfileActivity.this, Login.class);
                    startActivity(intent);
                    finish();
                }
                // ...
            }
        };
    }

    private void setCurrentEmail() {
        Log.d(TAG, "setCurrentEmail: setting current email to EditText field");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            Log.d(TAG, "setCurrentEmail: user is NOT null.");

          //  Toast.makeText(getApplicationContext(),"user email is"+user.getEmail()).show();
            String email = user.getEmail();






            Log.d(TAG, "setCurrentEmail: got the email: " + email);

            emailProfile.setText(email);

        }
    }


//    private void init(){
//
//        getUserAccountData();
//
//        saveProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick: attempting to save settings.");
//
//                //see if they changed the email
//                if(!emailProfile.getText().toString().equals(FirebaseAuth.getInstance().getCurrentUser().getEmail())){
//                    //make sure email and current password fields are filled
//                    if(!isEmpty(emailProfile.getText().toString())
////                          && !isEmpty(mCurrentPassword.getText().toString())
//                    )
//                           {
//
//                        //verify that user is changing to a company email address
//                        if(!emailProfile.getText().toString().equals("")){
////                            editUserEmail();
//                        }else{
//                            Toast.makeText(ProfileActivity.this, "Invalid Domain", Toast.LENGTH_SHORT).show();
//                        }
//
//                    }else{
//                        Toast.makeText(ProfileActivity.this, "Email and Current Password Fields Must be Filled to Save", Toast.LENGTH_SHORT).show();
//                    }
//                }
//
//
//                /*
//                ------ METHOD 1 for changing database data (proper way in this scenario) -----
//                 */
//                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
//                /*
//                ------ Change Name -----
//                 */
//                if(!addressProfile.getText().toString().equals("")){
//                    reference.child("users")
//                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
//                            .child("address")
//                            .setValue(addressProfile.getText().toString());
//                }
//
//
//                /*
//                ------ Change Phone Number -----
//                 */
//                if(!phoneProfile.getText().toString().equals("")){
//                    reference.child("users")
//                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
//                            .child("phone")
//                            .setValue(phoneProfile.getText().toString());
//                }
//
//
//
//
//            }
//        });
//
//        sendResetPasswordLink.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "onClick: sending password reset link");
//
//                /*
//                ------ Reset Password Link -----
//                */
//                sendResetPasswordLink();
//            }
//        });
//
//
//
//
//    }
//
//    private void getUserAccountData(){
//
//
//        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
//
//        /*
//            ---------- QUERY Method 1 ----------
//         */
//       // Query query1 = reference.child("users");
//
//        reference.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//
//                //this loop will return a single result
//                for(DataSnapshot singleSnapshot: dataSnapshot.getChildren()){
//
//
//                    User user11 = singleSnapshot.getValue(User.class);
//                     phoneProfile.setText(user11.getPhone());
//
//
////               String  setPPhone=dataSnapshot.child("address").getValue(String.class);
////                addressProfile.setText(setPPhone);
//
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//        /*
//            ---------- QUERY Method 2 ----------
//         */
//        Query query2 = reference.child("users")
//                .orderByChild("user_id")
//                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
//        query2.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                //this loop will return a single result
//                for(DataSnapshot singleSnapshot: dataSnapshot.getChildren()){
//
////                    String  setadd=dataSnapshot.child("address").getValue(String.class);
////                    addressProfile.setText(setadd);
//
//
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//        emailProfile.setText(FirebaseAuth.getInstance().getCurrentUser().getEmail());
//    }
    /**
     * Generalized method for asking permission. Can pass any array of permissions
     */
    public void verifyStoragePermissions(){
        Log.d(TAG, "verifyPermissions: asking user for permissions.");
        String[] permissions = {android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA};
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[0] ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[1] ) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this.getApplicationContext(),
                permissions[2] ) == PackageManager.PERMISSION_GRANTED) {
            mStoragePermissions = true;
        } else {
            ActivityCompat.requestPermissions(
                    ProfileActivity.this,
                    permissions,
                    REQUEST_CODE
            );
        }
    }
    private void sendResetPasswordLink(){
        FirebaseAuth.getInstance().sendPasswordResetEmail(FirebaseAuth.getInstance().getCurrentUser().getEmail())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "onComplete: Password Reset Email sent.");
                            Toast.makeText(ProfileActivity.this, "Sent Password Reset Link to Email",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            Log.d(TAG, "onComplete: No user associated with that email.");

                            Toast.makeText(ProfileActivity.this, "No User Associated with that Email.",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }
//    private void editUserEmail(){
//        // Get auth credentials from the user for re-authentication. The example below shows
//        // email and password credentials but there are multiple possible providers,
//        // such as GoogleAuthProvider or FacebookAuthProvider.
//
//        showDialog();
//
//        AuthCredential credential = EmailAuthProvider
//                .getCredential(FirebaseAuth.getInstance().getCurrentUser().getEmail(), mCurrentPassword.getText().toString());
//        Log.d(TAG, "editUserEmail: reauthenticating with:  \n email " + FirebaseAuth.getInstance().getCurrentUser().getEmail()
//                + " \n passowrd: " + mCurrentPassword.getText().toString());
//
//
//        FirebaseAuth.getInstance().getCurrentUser().reauthenticate(credential)
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        if(task.isSuccessful()){
//                            Log.d(TAG, "onComplete: reauthenticate success.");
//
//                            //make sure the domain is valid
//                            if(!(emailProfile.getText().toString()).equals("")){
//
//                                ///////////////////now check to see if the email is not already present in the database
//                                FirebaseAuth.getInstance().fetchProvidersForEmail(mEmail.getText().toString()).addOnCompleteListener(
//                                        new OnCompleteListener<ProviderQueryResult>() {
//                                            @Override
//                                            public void onComplete(@NonNull Task<ProviderQueryResult> task) {
//
//                                                if(task.isSuccessful()){
//                                                    ///////// getProviders().size() will return size 1 if email ID is in use.
//
//                                                    Log.d(TAG, "onComplete: RESULT: " + task.getResult().getProviders().size());
//                                                    if(task.getResult().getProviders().size() == 1){
//                                                        Log.d(TAG, "onComplete: That email is already in use.");
//                                                        hideDialog();
//                                                        Toast.makeText(ProfileActivity.this, "That email is already in use", Toast.LENGTH_SHORT).show();
//
//                                                    }else{
//                                                        Log.d(TAG, "onComplete: That email is available.");
//
//                                                        /////////////////////add new email
//                                                        FirebaseAuth.getInstance().getCurrentUser().updateEmail(emailProfile.getText().toString())
//                                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
//                                                                    @Override
//                                                                    public void onComplete(@NonNull Task<Void> task) {
//                                                                        if (task.isSuccessful()) {
//                                                                            Log.d(TAG, "onComplete: User email address updated.");
//                                                                            Toast.makeText(ProfileActivity.this, "Updated email", Toast.LENGTH_SHORT).show();
//                                                                            sendVerificationEmail();
//                                                                            FirebaseAuth.getInstance().signOut();
//                                                                        }else{
//                                                                            Log.d(TAG, "onComplete: Could not update email.");
//                                                                            Toast.makeText(ProfileActivity.this, "unable to update email", Toast.LENGTH_SHORT).show();
//                                                                        }
//                                                                        hideDialog();
//                                                                    }
//                                                                })
//                                                                .addOnFailureListener(new OnFailureListener() {
//                                                                    @Override
//                                                                    public void onFailure(@NonNull Exception e) {
//                                                                        hideDialog();
//                                                                        Toast.makeText(ProfileActivity.this, "unable to update email", Toast.LENGTH_SHORT).show();
//                                                                    }
//                                                                });
//
//
//                                                    }
//
//                                                }
//                                            }
//                                        })
//                                        .addOnFailureListener(new OnFailureListener() {
//                                            @Override
//                                            public void onFailure(@NonNull Exception e) {
//                                                hideDialog();
//                                                Toast.makeText(ProfileActivity.this, "unable to update email", Toast.LENGTH_SHORT).show();
//                                            }
//                                        });
//                            }else{
//                                Toast.makeText(ProfileActivity.this, "you must use a company email", Toast.LENGTH_SHORT).show();
//                            }
//
//                        }else{
//                            Log.d(TAG, "onComplete: Incorrect Password");
//                            Toast.makeText(ProfileActivity.this, "Incorrect Password", Toast.LENGTH_SHORT).show();
//                            hideDialog();
//                        }
//
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        hideDialog();
//                        Toast.makeText(ProfileActivity.this, "“unable to update email”", Toast.LENGTH_SHORT).show();
//                    }
//                });
//    }
    private void hideDialog(){
        if(mProgressBar.getVisibility() == View.VISIBLE){
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }
    public void sendVerificationEmail() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(ProfileActivity.this, "Sent Verification Email", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Toast.makeText(ProfileActivity.this, "Couldn't Verification Send Email", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

    }
    private void showDialog(){
        mProgressBar.setVisibility(View.VISIBLE);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }


}